package telecom;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.*;

public class KeyHandler {
    static int state = 0; // 0: pause, 1: start, 2: default

    public int getState(){
        return state; // 0: stop, 1: run, 2: default state
    }
    public void run() {

        JFrame frame = new JFrame("Key Listener");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        Container contentPane = frame.getContentPane();




        KeyListener listener = new KeyListener() {

            @Override

            public void keyPressed(KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.VK_ESCAPE)
                    state = 0;

                if (event.getKeyCode() == KeyEvent.VK_ENTER) {
                    state = 1;
                }

                if (event.getKeyCode() == KeyEvent.VK_SPACE)
                    state = 2;


                printEventInfo("Key Pressed", event);

            }

            @Override

            public void keyReleased(KeyEvent event) {

                printEventInfo("Key Released", event);

            }

            @Override

            public void keyTyped(KeyEvent event) {

                printEventInfo("Key Typed", event);

            }

            private void printEventInfo(String str, KeyEvent e) {

                System.out.println(str);

                int code = e.getKeyCode();

                System.out.println("   Code: " + KeyEvent.getKeyText(code));

                System.out.println("   Char: " + e.getKeyChar());

                int mods = e.getModifiersEx();

                System.out.println("    Mods: "

                        + KeyEvent.getModifiersExText(mods));

                System.out.println("    Location: "

                        + keyboardLocation(e.getKeyLocation()));

                System.out.println("    Action? " + e.isActionKey());

            }

            private String keyboardLocation(int keybrd) {

                switch (keybrd) {

                    case KeyEvent.KEY_LOCATION_RIGHT:

                        return "Right";

                    case KeyEvent.KEY_LOCATION_LEFT:

                        return "Left";

                    case KeyEvent.KEY_LOCATION_NUMPAD:

                        return "NumPad";

                    case KeyEvent.KEY_LOCATION_STANDARD:

                        return "Standard";

                    case KeyEvent.KEY_LOCATION_UNKNOWN:

                    default:

                        return "Unknown";

                }

            }

        };

        JTextField textField = new JTextField();

        textField.addKeyListener(listener);

        contentPane.add(textField, BorderLayout.NORTH);

        frame.pack();

        frame.setVisible(true);
    }
}
