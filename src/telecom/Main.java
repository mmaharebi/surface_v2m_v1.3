package telecom;

import java.io.IOException;
import java.util.Scanner;

public class Main {
    static Leap leapMotion = new Leap();
//    static Surface surface = new Surface();
    static Serial arduino = new Serial("COM3", 9600); // change comm port
    static DataHandler dataHandler = new DataHandler();
    static Scanner scanner = new Scanner(System.in);
//    static KeyListenerThread keyboardListener = new KeyListenerThread();
    static float[] normalVector;
    static int[] motorAngle = {84, 98};
    static byte[] motorAngleByte = dataHandler.intToByte(motorAngle);

    public static void main(String[] args) throws IOException, InterruptedException {
//        keyboardListener.run();
        leapMotion.connect();
        arduino.connect();
        boolean isValid = false;
        boolean hazard = false;

        while (true) {
            isValid = leapMotion.getValidity();
            if (isValid && !hazard){
                normalVector = leapMotion.getNormalVector();
                dataHandler.setNormalVector(normalVector);
                dataHandler.process();

//                System.out.println("Data:");
//                System.out.println(motorAngle[0]);
//                System.out.println(motorAngle[1]);
//                System.out.println('\n');

                motorAngle = dataHandler.getMotorAngle();
                motorAngleByte = dataHandler.intToByte(motorAngle);
                arduino.sendData(motorAngleByte);
//            arduino.sendData(motorAngleByte);
            }

            else if (!isValid)
                hazard = true;

            else if (hazard){
                while (!scanner.hasNext()){
                    hazard = true;
                }
                hazard = false;
                scanner = new Scanner(System.in);
            }

        }

    }

//    static void pause(){
//
//    }

//    static void run(){
//        normalVector = leapMotion.getNormalVector();
//        dataHandler.setNormalVector(normalVector);
//        dataHandler.process();
//
////        System.out.println("Data:");
////        System.out.println(motorAngle[0]);
////        System.out.println(motorAngle[1]);
////        System.out.println('\n');
//
//        motorAngle = dataHandler.getMotorAngle().clone();
//        motorAngleByte = dataHandler.intToByte(motorAngle);
//    }

//    static void horizental(){
//        motorAngle[0] = 84;
//        motorAngle[1] = 98;
//
//        motorAngleByte = dataHandler.intToByte(motorAngle);
//    }
}
