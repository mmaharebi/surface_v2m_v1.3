package telecom;
import com.leapmotion.leap.*;
import java.lang.Math.*;

class Leap {

    private Controller controller;
    private float[] normalVector = new float[3];
    private float phi;
    private float theta;
    private float[] handPosition = new float[3];

    float getPhi() {
        Frame frame = controller.frame();
        HandList hands = frame.hands();
        Hand hand = hands.get(0);
        if(hand.isValid()) {
            Vector normal_vector = hand.palmNormal().opposite();
            phi = 180 - normal_vector.yaw()*180f/(float)(Math.PI);
        }
        return phi;
    }

    float[] getNormalVector() {
        Frame frame = controller.frame();
        HandList hands = frame.hands();
        Hand hand = hands.get(0);
        if(hand.isValid()) {
            Vector normal_vector = hand.palmNormal().opposite();
            normalVector[0] = normal_vector.getZ();
            normalVector[1] = normal_vector.getX();
            normalVector[2] = normal_vector.getY();
        }
        return normalVector;
    }

    float getTheta() {
        Frame frame = controller.frame();
        HandList hands = frame.hands();
        Hand hand = hands.get(0);
        if(hand.isValid()) {
            Vector normal_vector = hand.palmNormal().opposite();
            theta = normal_vector.angleTo(Vector.yAxis())*180f/(float)(Math.PI);
        }
        return theta;
    }

    float[] getHandPosition(){
        Frame frame = controller.frame();
        HandList hands = frame.hands();
        Hand hand = hands.get(0);
        if(hand.isValid()) {
            Vector palmNormal = hand.palmNormal();
            handPosition[0] = palmNormal.getZ();
            handPosition[1] = palmNormal.getX();
            handPosition[2] = palmNormal.getY();
        }
        return handPosition;
    }

    boolean getValidity(){
        Frame frame = controller.frame();
        HandList hands = frame.hands();
        Hand hand = hands.get(0);
        return hand.isValid();

    }

    void connect(){
        controller = new Controller();
        while (!controller.isConnected());
        System.out.println("Leap Connected");
    }

}
