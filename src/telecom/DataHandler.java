package telecom;

public class DataHandler {
    private float[] normalVector = new float[3];

    private int[] motorAngle = new int[2];
    public int[] defaultMotorAngle = {84, 98};
    private int[][] angleRegion = {{40, 40}, {45, 50}};

    private float kx = 1f, ky = 1.5f;


    public void setNormalVector(float[] normalVector) {
        this.normalVector = normalVector;
    }

    public void setAngleRegion(int[][] angleRegion){
        this.angleRegion = angleRegion.clone();
    }

    public void process(){
        float[] temp = new float[2];

        float rx = (float)(normalVector[2]/Math.sqrt(Math.pow(normalVector[1], 2) + Math.pow(normalVector[2], 2)));
        float ry = (float)(normalVector[2]/Math.sqrt(Math.pow(normalVector[0], 2) + Math.pow(normalVector[2], 2)));

        if (normalVector[1] >= 0)
            temp[0] = (float)(Math.acos(rx)*180/Math.PI);
        else
            temp[0] = -(float)(Math.acos(rx)*180/Math.PI);

        if (normalVector[0] >= 0)
            temp[1] = -(float)(Math.acos(ry)*180/Math.PI);
        else
            temp[1] = (float)(Math.acos(ry)*180/Math.PI);

        temp[0] *= kx;
        temp[1] *= ky;

        if (temp[0] >= angleRegion[0][0])
            temp[0] = angleRegion[0][0];
        if (temp[0] <= -angleRegion[0][1])
            temp[0] = -angleRegion[0][1];

        if (temp[1] >= angleRegion[1][0])
            temp[1] = angleRegion[1][0];
        if (temp[1] <= -angleRegion[1][1])
            temp[1] = -angleRegion[1][1];

        temp[0] += defaultMotorAngle[0];
        temp[1] += defaultMotorAngle[1];

        motorAngle[0] = (int) temp[0];
        motorAngle[1] = (int) temp[1];


    }

    public int[] getMotorAngle(){
        return motorAngle;
    }

    public byte[] intToByte(int[] inputInteger) {
        String tempData;
        tempData = '<' + Integer.toString(inputInteger[0]) + ',' + Integer.toString(inputInteger[1]) + ',' + "0" + '>';
        return tempData.getBytes();
    }
}
