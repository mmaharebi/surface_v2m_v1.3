package telecom;

import com.fazecast.jSerialComm.*;

import java.io.IOException;
import java.io.OutputStream;

public class Serial {

    private final SerialPort arduinoPort;
    private MessageListener arduinoMessage = new MessageListener();
    private PacketListener arduinoPacket = new PacketListener();
    private final class MessageListener implements SerialPortMessageListener{
        @Override
        public int getListeningEvents() {
            return SerialPort.LISTENING_EVENT_DATA_RECEIVED | SerialPort.LISTENING_EVENT_DATA_WRITTEN;
        }

        @Override
        public byte[] getMessageDelimiter() {
            return new byte[] {(byte)0x3e};
        }

        @Override
        public boolean delimiterIndicatesEndOfMessage() {
            return true;
        }
        @Override
        public void serialEvent(SerialPortEvent serialPortEvent) {
            if (serialPortEvent.getEventType() == SerialPort.LISTENING_EVENT_DATA_RECEIVED) {
                byte[] newData = serialPortEvent.getReceivedData();
                System.out.println(new String(newData));
            } else if (serialPortEvent.getEventType() == SerialPort.LISTENING_EVENT_DATA_WRITTEN) {
                return;
            }
        }
    }
    private final class PacketListener implements SerialPortPacketListener{
        @Override
        public int getListeningEvents() {
            return SerialPort.LISTENING_EVENT_DATA_RECEIVED;
        }

        @Override
        public int getPacketSize() {
            return 32;
        }

        @Override
        public void serialEvent(SerialPortEvent serialPortEvent) {
            byte[] newData = serialPortEvent.getReceivedData();
            System.out.println(new String(newData));
        }
    }

    public Serial(String commPort, int baudrate) {
        this.arduinoPort = SerialPort.getCommPort(commPort);
        this.arduinoPort.setBaudRate(baudrate);
        this.arduinoPort.addDataListener(arduinoPacket);
    }

    void connect() throws InterruptedException{
        this.arduinoPort.openPort();
        Thread.sleep(500);
        if (arduinoPort.isOpen())
            System.out.println("Arduino is connected!");
        else
            System.out.println("Arduino is not connected!");
    }

    void sendData(byte[] data) throws IOException {
        if (data.length > 0) {
            OutputStream os = this.arduinoPort.getOutputStream();
            if (os != null) {
                os.write(data);
                os.flush();
            }
            else{
                System.out.println("Output Stream is null");
            }
        }
    }

    byte[] intToByte(int[] inputInteger){
        String tempData;
        tempData = '<' + Integer.toString(inputInteger[0]) + ',' + Integer.toString(inputInteger[1]) + ',' + Integer.toString(inputInteger[2]) + '>';
        return tempData.getBytes();
    }

}
